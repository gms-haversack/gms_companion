import 'package:flutter/material.dart';

class RollingList {
  String name;
  List<Item> items;

  RollingList({@required this.name, this.items = const []});

  RollingList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['items'] != null) {
      items = new List<Item>();
      json['items'].forEach((v) {
        items.add(new Item.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Item {
  String name;
  String description;
  String source;

  Item({@required this.name, this.source = "", this.description = ""});

  Item.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    source = json['source'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['description'] = this.description;
    data['source'] = this.source;
    return data;
  }

  String get formattedItemText {
    return '${this.name} (${this.source})\n\n${this.description}';
  }
}
