import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gms_companion/models/rollingList.dart';

class ItemInfoDialog extends StatelessWidget {
  final Item item;

  const ItemInfoDialog({
    Key key,
    this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text(item.name),
      contentPadding: EdgeInsets.all(1),
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.only(left:25, right: 25),
          title: Text(item.description),
        ),
        ButtonBar(
          children: <Widget>[
            FlatButton(
              child: const Icon(Icons.content_copy),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: item.formattedItemText));
                Navigator.pop(context, 0);
              },
            ),
            FlatButton(
              child: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context, 0);
              },
            ),
          ],
        )
      ],
    );
  }
}
