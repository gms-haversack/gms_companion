import 'package:flutter/material.dart';
import 'package:gms_companion/models/rollingList.dart';

class ListCreationDialog extends StatelessWidget {
  final TextEditingController _controller = TextEditingController();

  ListCreationDialog({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(1),
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.only(left: 25, right: 25),
        ),
        TextField(
          decoration: InputDecoration(
            labelText: 'List Name',
          ),
          controller: _controller,
        ),
        ButtonBar(
          children: <Widget>[
            FlatButton(
              child: const Icon(Icons.add),
              onPressed: () {
                Navigator.pop(
                    context, RollingList(name: _controller.text, items: []));
              },
            ),
            FlatButton(
              child: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        )
      ],
    );
  }
}
