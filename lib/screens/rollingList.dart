import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gms_companion/models/rollingList.dart';
import 'package:gms_companion/widgets/itemInfoDialog.dart';
import 'package:gms_companion/widgets/listCreationDialog.dart';

class ListSelectionPage extends StatefulWidget {
  @override
  _ListSelectionPageState createState() => _ListSelectionPageState();
}

class _ListSelectionPageState extends State<ListSelectionPage> {
  List<RollingList> lists = [];

  Future<void> _performListAction(list) async {
    switch (await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 0);
                },
                child: const Text('Get Random Item'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 2);
                },
                child: const Text('Get Specific Item'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 1);
                },
                child: const Text('View List'),
              ),
            ],
          );
        })) {
      case 0:
        Item item = (list.items.toList()..shuffle()).first;
        print(item.name);
        await showDialog<int>(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return ItemInfoDialog(item: item);
            });
        break;
      case 1:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ListViewPage(list: list)),
        );
        break;
    }
  }

  Future<void> _loadListCreation() async {
    RollingList newList = await showDialog<RollingList>(
        context: context,
        builder: (BuildContext context) {
          return ListCreationDialog();
        });
    setState(() {
      this.lists.add(newList);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Random Lists"),
      ),
      body: Center(
          child: ListView.builder(
              itemCount: this.lists.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                    title: Text(this.lists[index].name),
                    onTap: () {
                      _performListAction(this.lists[index]);
                    },
                  ))),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          this._loadListCreation();
          // Random _random = new Random();
          // dynamic x = {
          //   "name": _random.nextInt(500).toString(),
          //   "items": [
          //     {
          //       "name": "Handaxe blade",
          //       "description":
          //           "The blade appears to have been used as a chopping device",
          //       "source": "SKT pg.18",
          //     },
          //     {
          //       "name": "Dented helm",
          //       "description":
          //           "The helmet has telltale stains of some kind of broth",
          //       "source": "SKT pg.18",
          //     },
          //     {
          //       "name": "Moldy and stinky wheel of cheese",
          //       "description": "The cheesy has a few flies crawling around it",
          //       "source": "SKT pg.18",
          //     }
          //   ],
          // };
          // this.lists.add(RollingList.fromJson(x));
          // setState(() {});
        },
        tooltip: 'Add Item',
        child: Icon(Icons.add),
      ),
    );
  }
}

class ListViewPage extends StatefulWidget {
  ListViewPage({Key key, this.list}) : super(key: key);
  final RollingList list;

  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  @override
  Widget build(BuildContext context) {
    RollingList list = widget.list;
    return Scaffold(
      appBar: AppBar(),
      body: Center(
          child: ListView.builder(
              itemCount: list.items.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                    title: Text(list.items[index].name),
                  ))),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Random _random = new Random();
          list.items.add(Item(name: _random.nextInt(500).toString()));
          setState(() {});
        },
        tooltip: 'Add Item',
        child: Icon(Icons.add),
      ),
    );
  }
}
